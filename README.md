# Initial create with gatsby

Create container:
* docker build -t gatsby-initialize -f Dockerfile.initialize .
* docker run -it --name gatsby-initialize --rm -v `pwd`:/home/node gatsby-initialize bash

Run into the container
* gatsby new realestate-landing https://github.com/gatsbyjs/gatsby-starter-hello-world
