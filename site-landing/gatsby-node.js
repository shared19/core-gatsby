const path = require('path');

exports.createPages = async ({ graphql, actions }) => {
  const result = await graphql(`
    query {
      realEstate {
        properties {
          pk
        }
      }
    }
  `);

  const { createPage } = actions;

  result.data.realEstate.properties.forEach(property => {
    createPage({
      path: `/properties/${property.pk}/`,
      component: path.resolve(`./src/templates/property-details.js`),
      context: {
        propertyPk: property.pk,
      },
    })
  });
}
