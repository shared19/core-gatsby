// const axios = require("axios");

const REALESATE_TYPE = 'RealEstate';
const PROPERTY_TYPE = 'Property';


function createRealEstateNode({ createNodeId, createContentDigest, createNode, realEstateData }) {
  const nodeContent = JSON.stringify(realEstateData);
  const nodeMeta = {
    id: createNodeId(`${REALESATE_TYPE}-${realEstateData.pk}`),
    parent: null,
    children: [],
    internal: {
      type: REALESATE_TYPE,
      mediaType: `application/json`,
      content: nodeContent,
      contentDigest: createContentDigest(realEstateData),
    },
  }
  const node = Object.assign({}, realEstateData, nodeMeta);
  createNode(node);
}

function createPropertiesNode({ createNodeId, createContentDigest, createNode, propertiesData }) {

  propertiesData.forEach(propertyData => {
    const nodeContent = JSON.stringify(propertyData);
    const nodeMeta = {
      id: createNodeId(`${PROPERTY_TYPE}-${propertyData.pk}`),
      parent: null,
      children: [],
      internal: {
        type: PROPERTY_TYPE,
        mediaType: `application/json`,
        content: nodeContent,
        contentDigest: createContentDigest(propertyData),
      },
    }
    const node = Object.assign({}, propertyData, nodeMeta);
    createNode(node);
  })
}

exports.sourceNodes = async ({ actions, createNodeId, createContentDigest }) => {
  const { createNode } = actions;

  const realEstateId = process.env.REAL_ESTATE_ID;
  console.log('realEstateId -->', realEstateId);
  const serverIP = process.env.API_SERVER;
  // get data from our API
  // const response = await axios.get(`http://backend:5000/core/clientes/3/`);
  let response = {}
  response.data = realEstateId != 2 ?
    {
      "id": 2,
      "products": [
        {
          "id": 3,
          "pictures": [
            {
              "url": "http://lujanprop.com.ar/wp-content/uploads/2020/12/02.jpg"
            },
            {
              "url": "http://lujanprop.com.ar/wp-content/uploads/2020/12/04-768x1024.jpg"
            },
            {
              "url": "http://lujanprop.com.ar/wp-content/uploads/2020/12/03-768x1024.jpg"
            }
          ],
          "rooms": 2,
          "size": "42 m2",
          "garage": true,
          "tenant": 2
        },
        {
          "id": 4,
          "pictures": [
            {
              "url": "http://lujanprop.com.ar/wp-content/uploads/2021/01/SAM_0565-1024x768.jpg"
            }
          ],
          "rooms": 1,
          "size": "30 m2",
          "garage": false,
          "tenant": 2
        }
      ],
      "name": "Inmobiliario "+ realEstateId + " Propiedades",
      "phone": "(02323) 15-522268",
      "logo": "http://lujanprop.com.ar/wp-content/uploads/2019/03/Guazzaroni-500x500-300x300.png",
      "resume": "'Otro inmob."
    } :
    {
      "id": 2,
      "products": [
        {
          "id": 3,
          "pictures": [
            {
              "url": "http://lujanprop.com.ar/wp-content/uploads/2020/12/02.jpg"
            },
            {
              "url": "http://lujanprop.com.ar/wp-content/uploads/2020/12/04-768x1024.jpg"
            },
            {
              "url": "http://lujanprop.com.ar/wp-content/uploads/2020/12/03-768x1024.jpg"
            }
          ],
          "rooms": 2,
          "size": "42 m2",
          "garage": true,
          "tenant": 2
        },
        {
          "id": 4,
          "pictures": [
            {
              "url": "http://lujanprop.com.ar/wp-content/uploads/2021/01/SAM_0565-1024x768.jpg"
            }
          ],
          "rooms": 1,
          "size": "30 m2",
          "garage": false,
          "tenant": 2
        }
      ],
      "name": "Carolina Guazzaroni Inmobiliarios",
      "phone": "(02323) 15-522268",
      "logo": "http://lujanprop.com.ar/wp-content/uploads/2019/03/Guazzaroni-500x500-300x300.png",
      "resume": "'Soy María Carolina Guazzaroni con una extensa trayectoria en la educación, como Profesora para la enseñanza primaria y Maestra especializada en educación de jóvenes y adultos, emprendo un nuevo desafío en mi vida como Martillera y Corredera Pública universitaria. Porque entiendo que el crecimiento personal y cultural van de la mano y mi tarea comienza justo cuando finaliza el proyecto primogénito de mi vida.Una nueva y renovada etapa comienza, con la responsabilidad y el compromiso con el que he trabajado siempre, con la firme convicción de los valores éticos indispensables para el desarrollo de esta profesión. Al servicio de todos los lujaneros y pueblos vecinos! Compromiso de responsabilidad."
    };

  const propertiesData = response.data.products.map(prop => ({ ...prop, pk: prop.id }));

  const realEstateData = {
    ...response.data,
    pk: response.data.id,
    properties: propertiesData,
  };

  createRealEstateNode({ createNodeId, createContentDigest, createNode, realEstateData });
  createPropertiesNode({ createNodeId, createContentDigest, createNode, propertiesData });
};


exports.onPreInit = () => console.log("Loaded gatsby-source-realestate");
