import React from 'react';


export default function PropertyDetails({property}) {
  return (
    <div>
      <p><strong>Habitaciones:</strong> {property.rooms}</p>
      <p><strong>Cochera:</strong> {property.garage ? "Sí" : "No"}</p>
      <p><strong>Tamaño:</strong> {property.size}</p>
      {property.pictures && property.pictures.map &&
        <ul>
          {property.pictures.map(picture => <li key={picture}><img width="100px" src={picture} /></li>)}
        </ul>
      }
    </div>
  )
}
