import React from 'react';

const PropertyList = ({properties}) => {
  return (
    <ul>
      {properties.map(property => (
        <li key={property.pk}>
          {property.pictures && property.pictures.length > 0 &&
            <a href={`/properties/${property.pk}/`}>
              <img alt="alt" width="100px" src={property.pictures[0].url} />
            </a>
          }
          {(!property.pictures || property.pictures.length === 0) &&
            <a href={`/properties/${property.pk}/`}>
              Ver detalles (imágenes no disponibles)
            </a>
          }
        </li>
      ))}
    </ul>
  )
};


export default PropertyList;
