import React from "react";


export default function SearchProperty({roomCount, onSetRoomCount}) {
  return (
    <div>
      <label>
        Cantidad mínima de ambientes
        <input type="number" value={roomCount} onChange={(e) => onSetRoomCount(e.target.value)} />
      </label>
    </div>
  );
}
