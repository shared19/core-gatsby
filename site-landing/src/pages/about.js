import React from "react";
import { graphql } from "gatsby";

import Header from "../components/header";
import Container from "../components/container";
import Layout from "../components/layout";

export default function About({ data }) {
  return (
    <Layout>
      <Container>
        <Header headerText={`Quienes somos?`}/>
        <section>
          {data.realEstate.resume}
        </section>
      </Container>
    </Layout>
  )
}


export const query = graphql`
  query {
    realEstate {
      resume
    }
  }
`;
