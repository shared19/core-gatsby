import React from "react";
import { graphql } from "gatsby";

import Header from "../components/header";
import Layout from "../components/layout";

export default function Contact({ data }) {
  return (
    <Layout>
      <Header headerText={`Comunicate con ${data.realEstate.name}`} />
      <p>{data.realEstate.phone}</p>
    </Layout>
  )
}


export const query = graphql`
  query {
    realEstate {
      name
      phone
    }
  }
`;
