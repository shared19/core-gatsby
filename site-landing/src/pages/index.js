import React from "react";
import { graphql } from "gatsby";

import Header from "../components/header";
import Layout from "../components/layout";

export default function Home({ data }) {
  return (
    <Layout title={data.realEstate.name}>
      <Header headerText={`Bienvenidos a la página de ${data.realEstate.name}`} />
      <img src={data.realEstate.logo} alt="" />
    </Layout>
  )
};

export const query = graphql`
  query {
    realEstate {
      name
      logo
      phone
    }
  }
`;
