import React from "react";
import { graphql } from "gatsby";

import Header from "../components/header";
import Layout from "../components/layout";
import PropertyList from "../components/property_list";
import SearchProperty from "../components/search_property";


export default function Properties({ data }) {
  const [roomCount, setRoomCount] = React.useState(0);
  const filteredProperties = data.realEstate.properties.filter(
    property => property.rooms >= roomCount
  );
  return (
    <Layout title={data.realEstate.name}>
      <Header headerText={`Propiedades de ${data.realEstate.name}`} />
      <SearchProperty roomCount={roomCount} onSetRoomCount={setRoomCount} />
      <PropertyList properties={filteredProperties} />
    </Layout>
  )
};

export const query = graphql`
  query {
    realEstate {
      name
      properties {
        pk
        size
        rooms
        pictures {
          url
        }
        garage
      }
    }
  }
`;
