import React from "react";
import { graphql } from "gatsby";

import Header from "../components/header";
import Layout from "../components/layout";

export default function PropertyDetails({ data }) {
  console.log(`data === ${JSON.stringify(data)}`);
  return (
    <Layout title={data.realEstate.name}>
      <Header headerText={`Propiedades de ${data.realEstate.name}`} />
      <p><strong>Habitaciones:</strong> {data.property.rooms}</p>
      <p><strong>Cochera:</strong> {data.property.garage ? "Sí" : "No"}</p>
      <p><strong>Tamaño:</strong> {data.property.size}</p>
      {data.property.pictures &&
        <ul>
          {data.property.pictures.map(picture => <li key={picture}><img alt="alt" width="100px" src={picture.url} /></li>)}
        </ul>
      }
    </Layout>
  )
};

export const query = graphql`
  query($propertyPk: Int!) {
    property(pk: {eq: $propertyPk}) {
      garage
      pictures {
        url
      }
      rooms
      size
    }
    realEstate {
      name
    }
  }
`;
